import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
// Tailwind CSS Style Sheet
import "assets/styles/tailwind.css";
// React Toastify CSS Style Sheet
import "react-toastify/dist/ReactToastify.css";
import store from "redux/store";
import AllRoutes from "routes";

function App() {
  return (
    <Provider store={store}>
      <AllRoutes />
      <ToastContainer />
    </Provider>
  );
}

export default App;
