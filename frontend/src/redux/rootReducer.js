import { combineReducers } from "redux";
import memberReducer from "./member/memberReducer";
import authReducer from "./auth/authReducer";

const rootReducer = combineReducers({
  member: memberReducer,
  auth: authReducer
});

export default rootReducer;
