import axios from "axios";
import jwt_decode from "jwt-decode";
import { baseUrl } from "utils";
import {
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
} from "./authTypes";

// login action creators
const loginRequest = () => {
  return {
    type: LOGIN_REQUEST,
  };
};
const loginSuccess = (token) => {
  localStorage.setItem("token", token);
  let user = jwt_decode(token);
  return {
    type: LOGIN_SUCCESS,
    payload: user,
  };
};
const loginFailure = (error) => {
  return {
    type: LOGIN_FAILURE,
    payload: error,
  };
};

export const login = (authInfo) => {
  return function (dispatch) {
    dispatch(loginRequest());
    axios
      .post(`${baseUrl}/api/member/login`, authInfo)
      .then((res) => {
        dispatch(loginSuccess(res.data.token));
      })
      .catch((error) => {
        dispatch(loginFailure(error));
      });
  };
};

// logout action creators
const logoutSuccess = () => {
  localStorage.clear();
  return {
    type: LOGOUT_SUCCESS,
  };
};

export const logout = () => {
  return function (dispatch) {
    dispatch(logoutSuccess());
  };
};
