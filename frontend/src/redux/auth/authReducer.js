import jwt_decode from "jwt-decode";
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS,
} from "./authTypes";

const initialState = {
  isAuth: !!localStorage.getItem("token"),
  userInfo: !!localStorage.getItem("token")
    ? jwt_decode(localStorage.getItem("token"))
    : null,
  error: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        isAuth: false,
      };
    case LOGIN_SUCCESS:
      return {
        isAuth: true,
        userInfo: action.payload,
        error: "",
      };
    case LOGIN_FAILURE:
      return {
        isAuth: false,
        userInfo: null,
        error: action.payload,
      };
    case LOGOUT_SUCCESS:
      return {
        isAuth: false,
        userInfo: null,
        error: "",
      };
    default:
      return state;
  }
};

export default reducer;
