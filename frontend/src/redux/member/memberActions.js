import axios from "axios";
import { toast } from "react-toastify";
import { baseUrl } from "utils";
import {
  FETCH_MEMBER_FAILURE,
  FETCH_MEMBER_REQUEST,
  FETCH_MEMBER_SUCCESS,
  FILTER_MEMBER,
  DELETE_MEMBER_FAILURE,
  EDIT_MEMBER_FAILURE,
  EDIT_IMAGE_FAILURE,
  POST_MEMBER_FAILURE,
} from "./memberTypes";

//=============================================
//================ Fetch Members Action Creator
const fetchMemberRequest = () => {
  return {
    type: FETCH_MEMBER_REQUEST,
  };
};
const fetchMemberSuccess = (members) => {
  return {
    type: FETCH_MEMBER_SUCCESS,
    payload: members,
  };
};
const fetchMemberFailure = (error) => {
  return {
    type: FETCH_MEMBER_FAILURE,
    payload: error,
  };
};
// fetch all members
export const fetchMembers = () => {
  return function (dispatch) {
    dispatch(fetchMemberRequest());
    axios
      .get(`${baseUrl}/api/member/all`)
      .then((res) => {
        // console.log(data);
        dispatch(fetchMemberSuccess(res.data.data));
      })
      .catch((error) => {
        dispatch(fetchMemberFailure(error));
      });
  };
};
//=============================================
//================ Filter Members Action Creators
export const filterMember = (searchText) => {
  return {
    type: FILTER_MEMBER,
    payload: searchText.toLowerCase(),
  };
};

//=============================================
//================ Delete Member Action Creators
const deleteMemberFailure = () => {
  return {
    type: DELETE_MEMBER_FAILURE,
  };
};
export const deleteMember = (id) => {
  return function (dispatch) {
    axios
      .delete(`${baseUrl}/api/member/delete/${id}`)
      .then(() => {
        dispatch(fetchMembers());
        toast.success("Deleted successfully");
      })
      .catch(() => {
        dispatch(deleteMemberFailure());
        toast.error("Something wrong!!!");
      });
  };
};

//=============================================
//================ Update Member Action Creators
const updateMemberFailure = () => {
  return {
    type: EDIT_MEMBER_FAILURE,
  };
};
export const updateMember = (data, id) => {
  return function (dispatch) {
    axios
      .put(`${baseUrl}/api/member/update/${id}`, data)
      .then(() => {
        dispatch(fetchMembers());
        toast.success("Updated successfully");
      })
      .catch(() => {
        dispatch(updateMemberFailure());
        toast.error("Something wrong!!!");
      });
  };
};

//=============================================
//================ Update Image Action Creators
const updateImageFailure = () => {
  return {
    type: EDIT_IMAGE_FAILURE,
  };
};
export const updateImage = (data, id) => {
  return function (dispatch) {
    axios
      .put(`${baseUrl}/api/member/update/image/${id}`, { image: data })
      .then(() => {
        dispatch(fetchMembers());
        toast.success("Updated successfully");
      })
      .catch(() => {
        dispatch(updateImageFailure());
        toast.error("Something wrong!!!");
      });
  };
};

//=============================================
//================ Post Member Action Creators
const postMemberFailure = (error) => {
  return {
    type: POST_MEMBER_FAILURE,
    payload: error,
  };
};
export const postMember = (data) => {
  return function (dispatch) {
    axios
      .post(`${baseUrl}/api/member/add`, data)
      .then(() => {
        dispatch(fetchMembers());
        toast.success("Member added successfully");
      })
      .catch((error) => {
        dispatch(postMemberFailure(error));
        toast.error("Something wrong!!!");
      });
  };
};
