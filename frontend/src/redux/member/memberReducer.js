import {
  FETCH_MEMBER_FAILURE,
  FETCH_MEMBER_REQUEST,
  FETCH_MEMBER_SUCCESS,
  FILTER_MEMBER,
  DELETE_MEMBER_FAILURE,
  EDIT_MEMBER_FAILURE,
  EDIT_IMAGE_FAILURE,
  POST_MEMBER_FAILURE,
} from "./memberTypes";

const initialState = {
  loading: false,
  members: [],
  filterMember: [],
  error: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_MEMBER_REQUEST:
      return {
        loading: true,
      };
    case FETCH_MEMBER_SUCCESS:
      return {
        loading: false,
        members: action.payload,
        filterMember: action.payload,
        error: "",
      };
    case FETCH_MEMBER_FAILURE:
      return {
        loading: false,
        members: [],
        error: action.payload,
      };
    case POST_MEMBER_FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case FILTER_MEMBER:
      return {
        ...state,
        filterMember: state.members.filter(
          (ele) =>
            ele.firstName.toLowerCase().includes(action.payload) ||
            ele.id.includes(action.payload) ||
            ele.phone.includes(action.payload) ||
            ele.email.toLowerCase().includes(action.payload)
        ),
      };
    case DELETE_MEMBER_FAILURE:
    case EDIT_MEMBER_FAILURE:
    case EDIT_IMAGE_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default reducer;
