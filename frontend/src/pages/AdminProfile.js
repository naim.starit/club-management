import Layout from "components/layout";
import ProfileCard from "components/ProfileCard";
export default function AdminProfile() {
  return (
    <Layout>
      <div className="bg-light-blue-500 px-3 md:px-8 h-40" />
      <div className="px-3 md:px-8 h-auto">
        <ProfileCard />
      </div>
    </Layout>
  );
}
