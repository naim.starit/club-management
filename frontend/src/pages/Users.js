import Layout from "components/layout";
import UserTable from "components/user/UserTable";

export default function Users() {
  return (
    <Layout>
      <div className="px-3 md:px-8 h-auto mt-14">
        <div className="container mx-auto max-w-full">
          <div className="grid grid-cols-1 px-4 mb-16">
            <UserTable />
          </div>
        </div>
      </div>
    </Layout>
  );
}
