import Layout from "components/layout";

export default function Settings() {
  return (
    <Layout>
      <div className="px-3 md:px-8 h-auto mt-14">
        <div className="container mx-auto max-w-full">
          <p className="my-24 text-center">Settings page</p>
        </div>
      </div>
    </Layout>
  );
}
