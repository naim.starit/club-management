import React, { useState } from "react";
import Pagination from "@material-tailwind/react/Pagination";
import PaginationItem from "@material-tailwind/react/PaginationItem";
import Icon from "@material-tailwind/react/Icon";
import { Link } from "react-router-dom";

export default function PaginationComp({ data, postsPerPage }) {
  const [currentPage, setCurrentPage] = useState(1);

  let totalPage;
  if (data && data.length) totalPage = Math.ceil(data.length / postsPerPage);
  else totalPage = 1;
  // set next page
  const paginateFront = () => {
    if (totalPage > currentPage) {
      setCurrentPage(currentPage + 1);
    } else setCurrentPage(totalPage);
  };
  // set previous page
  const paginateBack = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };
  // for pagination button numbers
  let arrSize = totalPage - currentPage > 5 ? 4 : totalPage - currentPage;
  return (
    <>
      <Pagination>
        <Link to="/members/1">
          <button onClick={() => setCurrentPage(1)}>
            <PaginationItem className="cursor-pointer" button ripple="dark">
              First
            </PaginationItem>
          </button>
        </Link>
        <Link to={`/members/${currentPage > 1 ? currentPage - 1 : 1}`}>
          <button onClick={paginateBack}>
            <PaginationItem className="cursor-pointer" ripple="dark">
              <Icon name="keyboard_arrow_left" />
            </PaginationItem>
          </button>
        </Link>
        {new Array(arrSize + 1).fill("0").map((ele, index) => (
          <Link key={index} to={`/members/${index + currentPage}`}>
            <button onClick={() => setCurrentPage(index + currentPage)}>
              <PaginationItem
                className="cursor-pointer"
                color={index + currentPage === currentPage ? "lightBlue" : ""}
                ripple="dark"
              >
                {index + currentPage}
              </PaginationItem>
            </button>
          </Link>
        ))}
        <Link
          to={`/members/${
            currentPage < totalPage ? currentPage + 1 : totalPage
          }`}
        >
          <button onClick={paginateFront}>
            <PaginationItem className="cursor-pointer" ripple="dark">
              <Icon name="keyboard_arrow_right" />
            </PaginationItem>
          </button>
        </Link>
        <Link to={`/members/${totalPage}`}>
          <button onClick={() => setCurrentPage(totalPage)}>
            <PaginationItem className="cursor-pointer" button ripple="dark">
              Last
            </PaginationItem>
          </button>
        </Link>
      </Pagination>
      <div>
        <h2 className="text text-green-400">
          Showing page {currentPage} of {totalPage}
        </h2>
      </div>
    </>
  );
}
