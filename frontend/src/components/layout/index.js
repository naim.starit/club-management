import Sidebar from "components/layout/Sidebar";
import Footer from "components/layout/Footer";
function Layout({children}) {
  return (
    <>
      <Sidebar />
      <div className="md:ml-64">
       {children}
        <Footer />
      </div>
      </>
  );
}
export default Layout;
