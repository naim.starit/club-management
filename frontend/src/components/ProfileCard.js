import Card from "@material-tailwind/react/Card";
import CardBody from "@material-tailwind/react/CardBody";
import Image from "@material-tailwind/react/Image";
import H5 from "@material-tailwind/react/Heading5";
import LeadText from "@material-tailwind/react/LeadText";
import ProfilePicture from "assets/img/profile-image.jpg";

export default function ProfileCard() {
  return (
    <Card>
      <div className="flex flex-wrap justify-center">
        <div className="w-48 px-4 -mt-24">
          <Image src={ProfilePicture} rounded raised />
        </div>
      </div>
      <div className="text-center">
        <H5 color="gray">Super Admin</H5>
        <div className="mt-0 mb-2 text-gray-700 flex items-center justify-center gap-2">
          Club Management System
        </div>
      </div>
      <CardBody>
        <div className="text-left">
          <H5 color="gray">Gulshan Club History</H5>
        </div>
        <div className="border-t border-lightBlue-200 text-justify px-2 ">
          <LeadText color="blueGray">
            At the heart of Gulshan Club is the collective kinship amongst its
            Members. Founded in 1978, GCL prides itself for being more than just
            a club, but a home away from home. The Club was set up on the belief
            that life is best spent in the company of loved ones, and is
            presently the only family club in Bangladesh. Governing
            administrations, throughout the years, have been working
            relentlessly strengthening the bond and fellowship between its
            Members, as well as bringing families closer together through arts,
            culture, sports and various initiatives and events. From humble
            beginnings, early operations of the Club began from a simple
            thatched house which acted as the Club office, and Members getting
            together at each other’s residence. Owing to the creativity, passion
            and will of the Founding Members, the Club undertook a series of
            development projects, starting with the construction of the squash
            court and tennis complex, the club hosts a premise of nearly four
            and a half bighas (the biggest private plot in Gulshan), featuring
            two club houses and unparalleled state-of-the-art facilities. The
            Members of Gulshan Club are comprised of some of the most
            illustrious names in the country, which includes business leaders,
            bureaucrats, academicians, artists, bankers, journalists,
            celebrities, delegates, and public figures. Gulshan Club is
            considered to be one of the most elite clubs in Bangladesh.
          </LeadText>
        </div>
      </CardBody>
    </Card>
  );
}
