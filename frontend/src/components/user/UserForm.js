import { useState } from "react";
import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import Button from "@material-tailwind/react/Button";
import Input from "@material-tailwind/react/Input";
import { Image } from "@material-tailwind/react";
import profileImage from "assets/img/profile-image.jpg";
import { getBase64 } from "utils";
import { useDispatch } from "react-redux";
import { postMember } from "redux/member/memberActions";

export default function UserForm({ setShowModal }) {
  const [imageData, setImageData] = useState("");
  const [userName, setUserName] = useState("");
  const [userNameError, setUserNameError] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [emailError, setEmailError] = useState("");
  const [phone, setPhone] = useState("");
  const [phoneError, setPhoneError] = useState("");
  const [address, setAddress] = useState("");
  const [addressError, setAddressError] = useState("");
  const [city, setCity] = useState("");
  const [cityError, setCityError] = useState("");
  const [postCode, setPostCode] = useState("");
  const [company, setCompany] = useState("");
  const [position, setPosition] = useState("");
  const [profession, setProfession] = useState("");
  const [education, setEducation] = useState("");
  const [university, setUniversity] = useState("");
  // dispatch
  const dispatch = useDispatch();

  // image to base64 data convertion
  const fileUpload = (e) => {
    getBase64(e.target.files[0], (res) => {
      setImageData(res);
    });
  };
  // set error message
  const setErrorMessage = () => {
    if (!userName) setUserNameError("Username required");
    else setUserNameError("");
    if (!email) setEmailError("Email required");
    if (!phone) setPhoneError("Phone required");
    if (!address) setAddressError("Address required");
    if (!city) setCityError("City required");
  };
  // data validation
  const isValid = () => {
    if (userName && email && phone && address && city) return true;
    else {
      setErrorMessage();
      return false;
    }
  };
  // submit data
  const handleSubmit = (e) => {
    e.preventDefault();
    let data = {
      image: imageData,
      userName,
      firstName,
      lastName,
      email,
      phone,
      address,
      city,
      postCode,
      company,
      position,
      profession,
      education,
      university,
    };

    if (isValid()) {
      if (isValid()) {
        dispatch(postMember(data));
        setShowModal(false);
      }
    }
  };
  return (
    <Card>
      <CardHeader color="blue" contentPosition="none">
        <div className="w-full flex items-center justify-between">
          <h2 className="text-white text-2xl">Members Information</h2>
        </div>
      </CardHeader>
      <CardBody>
        <form>
          {/* ================ User Information ================ */}
          <h6 className="text-purple-500 text-sm mt-3 mb-6 font-light uppercase">
            User Information
          </h6>
          <div className="flex flex-wrap mt-10 items-center">
            <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
              <label
                htmlFor="files"
                className="text-purple-500 border-2 p-5 rounded-lg"
              >
                Select Image
              </label>
              <input
                onChange={fileUpload}
                style={{ visibility: "hidden" }}
                type="file"
                id="files"
              />
            </div>
            <div className="w-full lg:w-2/12 pr-4 mb-10 font-light">
              <div className="w-15 h-15 rounded-full border-2 border-white">
                <Image
                  src={imageData ? imageData : profileImage}
                  raised={false}
                  alt="profileimage"
                />
              </div>
            </div>
          </div>
          <div className="flex flex-wrap">
            <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
              <Input
                value={userName}
                onChange={(e) => setUserName(e.target.value)}
                type="text"
                color="purple"
                placeholder="Username"
                error={userName ? "" : userNameError}
              />
            </div>
            <div className="w-full lg:w-6/12 pl-4 mb-10 font-light">
              <Input
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                color="purple"
                placeholder="Email Address"
                error={email ? "" : emailError}
              />
            </div>
            <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
              <Input
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                type="text"
                color="purple"
                placeholder="First Name"
              />
            </div>
            <div className="w-full lg:w-6/12 pl-4 mb-10 font-light">
              <Input
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                type="text"
                color="purple"
                placeholder="Last Name"
              />
            </div>
          </div>
          {/* ================ User Contact Information ================ */}
          <h6 className="text-purple-500 text-sm my-6 font-light uppercase">
            Contact Information
          </h6>
          <div className="flex flex-wrap mt-10">
            <div className="w-full lg:w-12/12 mb-10 font-light">
              <Input
                value={address}
                onChange={(e) => setAddress(e.target.value)}
                type="text"
                color="purple"
                placeholder="Address"
                error={address ? "" : addressError}
              />
            </div>
            <div className="w-full lg:w-4/12 pr-4 mb-10 font-light">
              <Input
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
                type="text"
                color="purple"
                placeholder="Phone"
                error={phone ? "" : phoneError}
              />
            </div>
            <div className="w-full lg:w-4/12 px-4 mb-10 font-light">
              <Input
                value={city}
                onChange={(e) => setCity(e.target.value)}
                type="text"
                color="purple"
                placeholder="City"
                error={city ? "" : cityError}
              />
            </div>
            <div className="w-full lg:w-4/12 pl-4 mb-10 font-light">
              <Input
                value={postCode}
                onChange={(e) => setPostCode(e.target.value)}
                type="text"
                color="purple"
                placeholder="Postal Code"
              />
            </div>
          </div>
          {/* ================ Users Others Information ================ */}
          <h6 className="text-purple-500 text-sm my-6 font-light uppercase">
            Others Information (Job + Education)
          </h6>
          <div className="flex flex-wrap mt-10">
            <div className="w-full lg:w-6/12 pr-4 mb-10 font-light">
              <Input
                value={company}
                onChange={(e) => setCompany(e.target.value)}
                type="text"
                color="purple"
                placeholder="Company Name"
              />
            </div>
            <div className="w-full lg:w-6/12 pl-4 mb-10 font-light">
              <Input
                value={position}
                onChange={(e) => setPosition(e.target.value)}
                type="text"
                color="purple"
                placeholder="Position"
              />
            </div>
            <div className="w-full lg:w-4/12 pr-4 mb-10 font-light">
              <Input
                value={profession}
                onChange={(e) => setProfession(e.target.value)}
                type="text"
                color="purple"
                placeholder="Profession"
              />
            </div>
            <div className="w-full lg:w-4/12 px-4 mb-10 font-light">
              <Input
                value={education}
                onChange={(e) => setEducation(e.target.value)}
                type="text"
                color="purple"
                placeholder="Educational Qualifications"
              />
            </div>
            <div className="w-full lg:w-4/12 pl-4 mb-10 font-light">
              <Input
                value={university}
                onChange={(e) => setUniversity(e.target.value)}
                type="text"
                color="purple"
                placeholder="University"
              />
            </div>
          </div>
          <div>
            <Button onClick={handleSubmit} ripple="light">
              Submit
            </Button>
          </div>
        </form>
      </CardBody>
    </Card>
  );
}
