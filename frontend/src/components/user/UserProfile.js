import Card from "@material-tailwind/react/Card";
import CardBody from "@material-tailwind/react/CardBody";
import Image from "@material-tailwind/react/Image";
import H5 from "@material-tailwind/react/Heading5";
import Icon from "@material-tailwind/react/Icon";
import ProfilePicture from "assets/img/profile-image.jpg";
import { baseUrl } from "utils";

export default function UserProfile({ data }) {
  const {
    image,
    city,
    email,
    phone,
    firstName,
    userName,
    company,
    position,
    profession,
    education,
    university,
  } = data;
  return (
    <Card>
      <div className="flex flex-wrap justify-center">
        <div className="w-48 px-4 -mt-24">
          <Image
            src={image ? `${baseUrl}/images/${image}` : ProfilePicture}
            rounded
            raised
          />
        </div>
      </div>
      <div className="text-center">
        <H5 color="gray">{firstName}</H5>
        <div className="mt-0 mb-2 text-gray-700 flex items-center justify-center gap-2">
          <Icon name="place" size="xl" />
          {city}, Bangladesh
        </div>
        <div className="mb-2 text-gray-700 mt-10 flex items-center justify-center gap-2">
          <Icon name="work" size="xl" />
          {position} - {company}
        </div>
        <div className="mb-2 text-gray-700 flex items-center justify-center gap-2">
          <Icon name="school" size="xl" />
          {university}
        </div>
        <div className="mb-2 text-gray-700 flex items-center justify-center gap-2">
          <Icon name="phone" size="xl" />
          Phone: {phone}
        </div>
        <div className="mb-2 text-gray-700 flex items-center justify-center gap-2">
          <Icon name="mail" size="xl" />
          Email: {email}
        </div>
      </div>
      <div className="w-full my-4 font-light items-center border border-gray-200"></div>
      <div className="w-full flex items-center justify-between">
        <h2 className="text-black text-xl font-medium">More Information:</h2>
      </div>
      <div className="w-full">
        <table className="lg:w-6/12 sm:w-full md:w-full border-collapse">
          <tbody>
            <tr>
              <td className="font-medium">Username</td>
              <td>{userName}</td>
            </tr>
            <tr>
              <td className="font-medium">Prefession</td>
              <td>{profession}</td>
            </tr>
            <tr>
              <td className="font-medium">Education</td>
              <td>{education}</td>
            </tr>
            <tr>
              <td className="font-medium">University</td>
              <td>{university}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div className="w-full mt-4 font-light items-center border border-gray-200"></div>
      <CardBody>
        <div className="w-full flex items-center justify-between">
          <h2 className="text-black text-xl font-medium">{firstName} says-</h2>
        </div>
        <div className="w-full lg:w-12/12 bg-blue-200 ">
          <p className="break-words p-5 border-2 rounded-xl">
            This is {firstName}, rising computer engineer from Bangladesh.{" "}
            <br></br> My background is in Computer Science and Engineering from
            Eastern University.<br></br> I am doing JavaScript, Node.js,
            React.js. I love JS and all technologies <br /> of JavaScript very
            much. Now I am working in a company as a MERN Stack developer.{" "}
            <br /> I also love to solve problem from different OJ such as
            Codeforces, codechef, hackerrank, lightoj etc.
          </p>
        </div>
      </CardBody>
    </Card>
  );
}
