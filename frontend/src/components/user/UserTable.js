import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Card from "@material-tailwind/react/Card";
import CardHeader from "@material-tailwind/react/CardHeader";
import CardBody from "@material-tailwind/react/CardBody";
import Button from "@material-tailwind/react/Button";
import { CardFooter, Image, InputIcon } from "@material-tailwind/react";
import ProfileImage from "assets/img/profile-image.jpg";
import ModalComp from "../Modal";
import UserForm from "./UserForm";
import ModalAlertComp from "../ModalAlert";
import UserProfile from "./UserProfile";
import { fetchMembers, filterMember } from "redux/member/memberActions";
import { baseUrl } from "utils";
import UserEdit from "./UserEdit";
import Pagination from "../Pagination";
import { useHistory } from "react-router-dom";

export default function UserTable() {
  // state declaration ========================
  const [searchText, setSearchText] = useState("");
  const [showModalAlert, setShowModalAlert] = useState(false);
  const [showModalProfile, setShowModalProfile] = useState(false);
  const [editInfo, setEditInfo] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [row, setRow] = useState({});
  const [postsPerPage, setPostsPerPage] = useState(10);
  const dispatch = useDispatch();
  const { page } = useParams();
  const history = useHistory();
  // fetch data from redux state ========================
  const { filterMember: members, loading } = useSelector(
    (state) => state.member
  );
  // ======= set filtered member
  useEffect(() => {
    dispatch(filterMember(searchText));
    history.push("/members/1");
  }, [searchText, dispatch, history]);

  // if(membersData) setMembers(membersData);

  useEffect(() => {
    dispatch(fetchMembers());
  }, [dispatch]);

  // member info view handler ==========================
  const handleView = (row) => {
    setShowModalProfile(true);
    setRow(row);
  };
  // member info edit handler =======================
  const handleEdit = (row) => {
    setEditInfo(true);
    setRow(row);
  };

  return (
    <>
      {!editInfo ? (
        <Card>
          <CardHeader color="blue" contentPosition="none">
            <div className="w-full flex items-center justify-between">
              <h2 className="text-white text-2xl">Club Members</h2>
              <Button
                type="button"
                onClick={() => setShowModal(true)}
                ripple="light"
              >
                + Add Member
              </Button>
              <ModalComp showModal={showModal} setShowModal={setShowModal}>
                <UserForm setShowModal={setShowModal} />
              </ModalComp>
            </div>
          </CardHeader>
          <CardBody>
            {/* =================== member search section ===================== */}
            <div className="flex justify-center mb-4">
              <div className="w-80">
                <InputIcon
                  type="text"
                  color="lightBlue"
                  value={searchText}
                  onChange={(e) => setSearchText(e.target.value)}
                  size="regular"
                  outline={true}
                  placeholder="search by id/name/phone/email"
                  iconFamily="material-icons"
                  iconName="search"
                />
              </div>
            </div>
            {/* ===================== table body start ===================== */}
            <div className="overflow-x-auto">
              {!loading ? (
                <table className="items-center w-full bg-transparent border-collapse">
                  <thead>
                    <tr>
                      <th className="px-2 text-teal-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                        ID
                      </th>
                      <th className="px-2 text-teal-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                        Image
                      </th>
                      <th className="px-2 text-teal-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                        Name
                      </th>
                      <th className="px-2 text-teal-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                        Phone
                      </th>
                      <th className="px-2 text-teal-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                        View
                      </th>
                      <th className="px-2 text-teal-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                        Edit
                      </th>
                      <th className="px-2 text-teal-500 align-middle border-b border-solid border-gray-200 py-3 text-sm whitespace-nowrap font-light text-left">
                        Delete
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {members &&
                      members.length &&
                      members
                        .slice((page - 1) * postsPerPage, page * postsPerPage)
                        .map((ele) => (
                          <tr key={ele.id}>
                            <th className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                              {ele.id}
                            </th>
                            <td className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                              <div className="w-10 h-10 rounded-full border-2 border-white -ml-4">
                                <Image
                                  src={
                                    ele.image
                                      ? `${baseUrl}/images/${ele.image}`
                                      : ProfileImage
                                  }
                                  alt="..."
                                />
                              </div>
                            </td>
                            <td className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                              {ele.firstName}
                            </td>
                            <td className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                              {ele.phone}
                            </td>
                            {/* ================= View an item =============== */}
                            <td className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                              <Button
                                color="teal"
                                type="button"
                                onClick={() => handleView(ele)}
                                ripple="light"
                              >
                                View
                              </Button>
                              <ModalComp
                                showModal={showModalProfile}
                                setShowModal={setShowModalProfile}
                              >
                                <UserProfile data={row} />
                              </ModalComp>
                            </td>
                            {/* ================= edit an item =============== */}
                            <td className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                              <Button
                                type="button"
                                onClick={() => handleEdit(ele)}
                                ripple="light"
                              >
                                Edit
                              </Button>
                            </td>
                            {/* =================delete an item=============== */}
                            <td className="border-b border-gray-200 align-middle font-light text-sm whitespace-nowrap px-2 py-4 text-left">
                              <Button
                                onClick={() => {
                                  setShowModalAlert(true);
                                  setRow(ele);
                                }}
                                className="bg-red-500"
                              >
                                Delete
                              </Button>
                              <ModalAlertComp
                                showModalAlert={showModalAlert}
                                setShowModalAlert={setShowModalAlert}
                                data={row}
                              />
                            </td>
                          </tr>
                        ))}
                  </tbody>
                </table>
              ) : (
                // loading state
                <h1>Loading........</h1>
              )}
            </div>
          </CardBody>
          {/* =====================Pagination section==================== */}
          <CardFooter className="flex flex-col justify-center items-center">
            <Pagination
              data={members ? members : []}
              postsPerPage={postsPerPage}
            />
          </CardFooter>
        </Card>
      ) : (
        <UserEdit data={row} setEditInfo={setEditInfo} />
      )}
    </>
  );
}
