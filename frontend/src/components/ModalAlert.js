import React from "react";
import Modal from "@material-tailwind/react/Modal";
import ModalHeader from "@material-tailwind/react/ModalHeader";
import ModalBody from "@material-tailwind/react/ModalBody";
import ModalFooter from "@material-tailwind/react/ModalFooter";
import Button from "@material-tailwind/react/Button";
import { useDispatch } from "react-redux";
import { deleteMember } from "redux/member/memberActions";

export default function ModalAlertComp({
  showModalAlert,
  setShowModalAlert,
  data,
}) {
  const dispatch = useDispatch();
  const handleDelete = () => {
    dispatch(deleteMember(data.id));
    setShowModalAlert(false);
  };
  return (
    <>
      {/* <Button
        color="lightBlue"
        type="button"
        onClick={(e) => setShowModalAlert(true)}
        ripple="light"
      >
        Delete an Item
      </Button> */}

      <Modal
        size="regular"
        active={showModalAlert}
        toggler={() => setShowModalAlert(false)}
      >
        <ModalHeader toggler={() => setShowModalAlert(false)}>
          Delete an Item
        </ModalHeader>
        <ModalBody>
          <p className="text-base leading-relaxed text-gray-600 font-normal">
            Are you Sure?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button
            color="green"
            onClick={(e) => setShowModalAlert(false)}
            ripple="light"
          >
            Cancel
          </Button>
          <Button
            color="red"
            buttonType="link"
            onClick={handleDelete}
            ripple="dark"
          >
            Delete
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
}
