import React from "react";
import { useSelector } from "react-redux";
import { Route } from "react-router-dom";
import { Redirect } from "react-router-dom";

const AuthRoute = (props) => {
  const { isAuth } = useSelector((state) => state.auth);
  if (isAuth) {
    return <Route {...props} />;
  }
  return <Redirect to="/login" />;
};

export default AuthRoute;
