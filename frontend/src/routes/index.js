import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Login from "pages/Login";
import AdminProfile from "pages/AdminProfile";
import Users from "pages/Users";
import Settings from "pages/Settings";
import AuthRoute from "./AuthRoute";
import { useSelector } from "react-redux";
const AllRoutes = () => {
  const { isAuth } = useSelector((state) => state.auth);
  return (
    <Switch>
      <AuthRoute exact path="/" component={AdminProfile} />
      <AuthRoute path="/members/:page" component={Users} />
      <AuthRoute path="/settings" component={Settings} />
      {!isAuth && <Route path="/login" component={Login} />}
      <Redirect from="*" to="/" />
    </Switch>
  );
};

export default AllRoutes;
