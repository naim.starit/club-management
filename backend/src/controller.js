//node package manager
const { customAlphabet } = require("nanoid");
var jwt = require("jsonwebtoken");
//services
const { getJsonData, writeJsonData, updateJsonData } = require("./services");
const { base64ToImage, removeImage } = require("./utils");
const privateKey = "kdfji45dfe54sdfejdkfjnaimhossenfullstack";
//to get all members data
const getAll = (request, response) => {
  try {
    // get data from database
    let data = getJsonData(__dirname, "db.json");
    // if data available
    if (data.length) {
      setTimeout(() => {
        return response.status(200).send({ data });
      }, 100);
    }
    // if no data available
    else {
      setTimeout(() => {
        return response
          .status(400)
          .send({ message: "No data found", data: [] });
      }, 100);
    }
  } catch (error) {
    response.status(400).send({ error: error.message });
  }
};

// =============== add new member controller ===============
const create = (request, response) => {
  try {
    let imageName;
    let uniqueId = customAlphabet("1234567890", 6);
    if (request.body.image) {
      imageName = base64ToImage(request.body.image);
      delete request.body.image;
    }

    let data = {
      ...request.body,
      id: uniqueId(),
      image: !!imageName ? imageName : "image.png",
    };
    // add new data to database
    writeJsonData(__dirname, "db.json", data);
    // send response
    setTimeout(function () {
      return response.status(201).send({ message: "Data added successfully" });
    }, 100);
  } catch (error) {
    response.status(400).send({ error: error.message });
  }
};

//=================== Delete member Controller =========================

const deleteMember = (request, response) => {
  try {
    const id = request.params.id;
    // get all data
    let data = getJsonData(__dirname, "db.json");
    // if data available
    if (data.length) {
      // remove member with params id
      let newData = data.filter((ele) => {
        if (ele.id === id) {
          // remove previous image without default images
          if (ele.image !== "image.png" && ele.image !== "profile-image.jpg") {
            removeImage(ele.image);
          }
        }
        return ele.id !== id;
      });
      // updatae database
      updateJsonData(__dirname, "db.json", newData);
      //send response
      setTimeout(function () {
        return response
          .status(204)
          .send({ message: "Data deleted successfully" });
      }, 100);
    }
  } catch (error) {
    response.status(400).send({ error: error.message });
  }
};

// to update member information
const updateMember = (request, response) => {
  try {
    let updatedData;
    const id = request.params.id;
    // get all data
    let data = getJsonData(__dirname, "db.json");
    // find member with params id
    if (data.length) {
      let presentData = data.find((ele) => ele.id === id);
      updatedData = {
        id,
        image: presentData.image,
        ...request.body,
      };
      let dataIndex = data.findIndex((ele) => ele.id === id);
      data.splice(dataIndex, 1, updatedData);
      // updatae database
      updateJsonData(__dirname, "db.json", data);
      // send response
      setTimeout(function () {
        return response
          .status(204)
          .send({ message: "Data updated successfully" });
      }, 100);
    }
  } catch (error) {
    response.status(400).send({ error: error.message });
  }
};

// ==================== Update Profile Picture Controller =======================
const updateProfileImage = (request, response) => {
  try {
    let imageName;
    const id = request.params.id;
    // get all data
    let data = getJsonData(__dirname, "db.json");
    //save image to directory and get image name
    imageName = base64ToImage(request.body.image);
    // find member with params id
    if (data.length) {
      let presentData = data.find((ele) => ele.id === id);
      // remove previous image without default images
      if (
        presentData.image !== "image.png" &&
        presentData.image !== "profile-image.jpg"
      ) {
        removeImage(presentData.image);
      }
      // set new image name
      presentData.image = imageName;
      let dataIndex = data.findIndex((ele) => ele.id === id);
      data.splice(dataIndex, 1, presentData);
      // updatae database
      updateJsonData(__dirname, "db.json", data);
      // send response
      setTimeout(function () {
        return response
          .status(204)
          .send({ message: "Image updated successfully" });
      }, 100);
    }
  } catch (error) {
    response.status(400).send({ error: error.message });
  }
};

//=============== Login Controller =======================

const login = (request, response) => {
  try {
    // static userName and password
    let userName, pass;
    (userName = "admin@admin.com"), (pass = "admin");
    // check userName and password
    const { user, password } = request.body;
    if (user === userName && password === pass) {
      let token = jwt.sign({ user }, privateKey);
      setTimeout(function () {
        return response.status(200).send({ token });
      }, 100);
    } else {
      setTimeout(function () {
        return response.status(401).send({ message: "User not found" });
      }, 100);
    }
  } catch (error) {
    response.status(400).send({ error: error.message });
  }
};

module.exports = {
  getAll,
  create,
  updateMember,
  updateProfileImage,
  deleteMember,
  login,
};
