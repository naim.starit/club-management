const express = require("express");
const router = express.Router();

const {
  getAll,
  create,
  deleteMember,
  updateMember,
  updateProfileImage,
  login,
} = require("./controller");

router.get("/all", getAll);
router.post("/add", create);
router.put("/update/:id", updateMember);
router.put("/update/image/:id", updateProfileImage);
router.delete("/delete/:id", deleteMember);

// auth routes
router.post("/login", login);

module.exports = router;
