const fs = require("fs");
// convert base64 data to image file and return image name
exports.base64ToImage = (image) => {
  // data:image/png;base64,iVBORw0KGgoAAAANSUhEUg......
  let data = image.split(";base64,");
  let base64Data = data[1];
  let imageExtension = data[0].split("/")[1];
  let fileName = `profile-image${+new Date()}.${imageExtension}`;
  fs.writeFile(
    `${__dirname}/../images/${fileName}`,
    base64Data,
    { encoding: "base64" },
    function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log("File has been saved successfully!!!");
      }
    }
  );
  return fileName;
};

exports.removeImage = (image) => {
  fs.unlink(`${__dirname}/../images/${image}`, function (err) {
    if (err) {
      console.log(err);
    } else {
      console.log("File removed successfully");
    }
  });
};
