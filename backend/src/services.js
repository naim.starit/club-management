const path = require("path");
const fs = require("fs");

exports.getJsonData = function (basePathToData, filename) {
  var filename = path.join(basePathToData, filename);
  return JSON.parse(fs.readFileSync(filename, "utf-8"));
};

exports.writeJsonData = function (basePathToData, filename, data) {
  var filename = path.join(basePathToData, filename);
  const jsonData = JSON.parse(fs.readFileSync(filename, "utf-8"));
  jsonData.unshift(data);
  const newData = JSON.stringify(jsonData);
  fs.writeFile(filename, newData, (err) => {
    if (err) throw err;
  });
};

exports.updateJsonData = function (basePathToData, filename, data) {
  var filename = path.join(basePathToData, filename);
  const newData = JSON.stringify(data);
  fs.writeFile(filename, newData, (err) => {
    if (err) throw err;
  });
};
