const express = require("express");
const cors = require("cors");
const app = express();
const path = require("path");
app.use(cors());
app.use(express.static(path.join(__dirname, "src")));
app.use(express.json({ limit: "10mb" }));

const memberRoutes = require("./src/routes");
app.use("/api/member", memberRoutes);

app.get("*", (request, response) => {
  response.send({ message: "API not found" });
});

const port = 4000;
app.listen(port, () => console.log("server is listening on port", port));
