# Club Management Application Description and Instructions

# Description 
Simple club management applictaion.

**features** 

- Node provides the backend environment for this application
- Express middleware is used to handle requests, routes
- JWT to handle authentication 
- JSON file to model the application data
- React for displaying UI components
- Tailwind for UI design
- Redux to manage application's state
- Redux Thunk middleware to handle asynchronous redux actions

# Demo Video 

See the application [Link](https://youtu.be/D9O8hnz-PYM)

# Install 
Some basic commands are
1. git clone https://gitlab.com/naim.starit/club-management.git 
2. cd club-management 
3. yarn
4. yarn installAll  (to install backend and frontend packages) 


# Run the application 
yarn start 
(this command will start frontend and backend concurrently)

# Language and tools 
- Node 
- Express 
- React 
- Tailwind 
- React-redux 
- JWT

# App Description 
- Admin can login and logout
- Admin can view all members 
- Admin can view specific member 
- Admin can add member 
- Admin can update member's information 
- Admin can update member's profile image 
- Admin can delete member 
- Admin can search member by name, phone, userId and email 


